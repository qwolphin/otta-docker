## Dockerfiles for Otta + Docker images

The images are made to be used in CI contexts

### Build

```bash
docker build -t wolphin/otta:latest .
docker push wolphin/otta:latest
```

```bash
docker build -t wolphin/otta:dind --file Dockerfile-dind .
docker push wolphin/otta:dind
```
